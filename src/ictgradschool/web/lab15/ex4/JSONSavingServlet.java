package ictgradschool.web.lab15.ex4;


import jdk.management.resource.internal.ResourceNatives;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import sun.font.Script;

import javax.lang.model.element.VariableElement;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.*;
import java.security.acl.Owner;
import java.util.Map;
import java.util.function.Function;


public class JSONSavingServlet extends HttpServlet {
    private String transactionDir;
    private String filePath;

    /**
     *
     * @param servletConfig
     *            a ServletConfig object containing information gathered when
     *            the servlet was created, including information from web.xml
     */
    public void init(ServletConfig servletConfig) throws ServletException{
        super.init(servletConfig);
        this.transactionDir = servletConfig.getInitParameter("invoice_No");            filePath = getServletContext().getRealPath(transactionDir);
        File file = new File(filePath);
        file.mkdir();

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        /* In this method, retrieve the submitted parameters and generate   *
         * a JSON document to represent the data. An example of creating    *
         * a JSON document using the json-simple library can be seen below. */

        /* If any parameter is missing, redirect the user to the form again *
         * by calling the doGet(request, response); method. Use those       *
         * parameters to populate the form, and indicate the missing values */
        JSONObject transaction = new JSONObject();
        JSONObject billing = new JSONObject();
        JSONObject card = new JSONObject();
        Map<String, String[]> results = request.getParameterMap();
        for(String strings: results.keySet()){
            if(results.get(strings)[0].equals("")) {
                doGet(request, response);
            }
            else{
                if(strings.equals("billing")){
                    billing.put(strings, results.get(strings)[0]);
                }
                else if(!strings.equals("Submit Query")){
                    card.put(strings, results.get(strings)[0]);
                }
            }
        }

        transaction.put("billingAddress", billing);
        transaction.put("creditCard", card);


        System.out.println(transaction.toJSONString());

        File file = new File(filePath,results.get("invoice_No")[0] + ".json");

        saveJSONObject(file, transaction);
        response.sendRedirect("/question4");
        // Adds a String value to the JSON





        /* Save the JSON object to an appropriate directory, using the    *
         * 'invoice number' parameter as the filename, with the extension *
         * '.json'. An example filename might be '15678.json'. The method *
         * saveJSONObject() has been provided to save the JSON            */
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter pw = response.getWriter();

        String invoice_No="";
        String billing="";
        String cardHolder="";
        String cardProvider="";
        String cardNo="";
        if(request.getParameter("invoice_No")!=null)
        {
            invoice_No =  request.getParameter("invoice_No");
        };
        if(request.getParameter("billing")!=null){
            billing = request.getParameter("billing");
        }
        if(request.getParameter("cardHolder")!=null)
        {
            cardHolder =  request.getParameter("cardHolder");
        };
        if(request.getParameter("cardProvider")!=null){
            cardProvider = request.getParameter("cardProvider");
        }
        if(request.getParameter("cardNo")!=null){
            cardNo = request.getParameter("cardNo");
        }
        pw.println("<body onload='highlight()'>");
        pw.print("<form action='/question4' method='post'>");
        pw.println("Invoice #: ");
        pw.println("<input type='text' name='invoice_No' value='" + invoice_No +"'></br>");
        pw.println("Billing Address:</br>");
        pw.println("<textarea name='billing' rows='10' cols='30' >"+ billing+ "</textarea></br>");
        pw.println("Name of cardholder:</br>");
        pw.println("<input type='text' name='cardHolder' value='"+ cardHolder +"'></br>");
        pw.println("Card Provider:</br>");
        pw.println("<select name='cardProvider' value='"+ cardProvider +"'>");
        pw.println("<option value='Visa'>Visa</option>");
        pw.println("<option value='MCard'>MasterCard</option>");
        pw.println("<option value='AmEx'>American Express</option></select></br>");
        pw.println("Card Number:");
        pw.println("<input type='text' name='cardNo' size='16' value='"+ cardNo +"'></br>");
        pw.println("<input type='submit' name='Submit'>");
        pw.print("</form>");
       pw.println("<script>function highlight(){ " +
               "var text = document.getElementsByTagName('textarea');" +
                       "if(text[0].innerHTML == ''){" +
                       "text[0].style.backgroundColor = 'yellow'};" +
               "var elem = document.getElementsByTagName('input'); " +
               "for(var i = 0; i < elem.length; i++){ " +
               "if(elem[i].value == ''){ " +
                "elem[i].style.backgroundColor = 'yellow'; " +
               "} " +
               "} " +
               "} </script></body>");
        pw.close();
    }

    /**
     * Writes the given JSONObject (in JSON format) to the specified file path
     *
     * @param file
     * @param jsonRecord
     * @return true if file written successfully, false otherwise
     */
    private boolean saveJSONObject(File file, JSONObject jsonRecord) {
        if (file.exists()) {
            return false;
        }

        try (BufferedWriter writer = new BufferedWriter( new FileWriter(file))) {
            writer.write(JSONObject.toJSONString(jsonRecord));
        } catch (IOException e) {
            return false;
        }

        return true;
    }
}
