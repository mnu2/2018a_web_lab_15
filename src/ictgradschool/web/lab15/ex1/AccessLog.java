package ictgradschool.web.lab15.ex1;

import java.sql.Date;

public class AccessLog {
    private int id;
    private String name;
    private String description;
    private Date timestamp;

    public int getId(){
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setId(int id){
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setTimestamp(Date timestamp){
        this.timestamp = timestamp;
    }
}
