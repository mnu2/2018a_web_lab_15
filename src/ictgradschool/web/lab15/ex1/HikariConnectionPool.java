package ictgradschool.web.lab15.ex1;

import com.zaxxer.hikari.HikariDataSource;

import java.sql.SQLException;


import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;

import java.util.Properties;

public class HikariConnectionPool {
    private static HikariDataSource hds;
    static Properties dbProps = new Properties();

    public static void setup(String path) {
        if (hds != null) { return ; }
        
    
        dbProps = new Properties();
        try (FileInputStream fIn = new FileInputStream(path)) {
            dbProps.load(fIn);
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        hds = new HikariDataSource();
        hds.setMaximumPoolSize(2);
        hds.setDriverClassName(dbProps.getProperty("driver"));
        hds.setJdbcUrl(dbProps.getProperty("url"));
        hds.setUsername(dbProps.getProperty("user"));
        hds.setPassword(dbProps.getProperty("password"));
        hds.addDataSourceProperty("useSSL", dbProps.getProperty("useSSL"));
    }

    public static Connection getConnection() throws SQLException {
        return hds.getConnection();
    }
}
