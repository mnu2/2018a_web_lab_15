package ictgradschool.web.lab15.ex1;


import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class AccessLogDAO {

    public void start(){
//        insertEntry("Lara", "Signed up for the news Letter");
//        System.out.println(loadLog().size());
//        deleteEntry("Lara");
    }

    public static List<AccessLog> loadLog(){
        List<AccessLog> accessLogDAO = new ArrayList<>();
        try(Connection connection = HikariConnectionPool.getConnection()){
            try(Statement stmt = connection.createStatement()){
                try(ResultSet resultSet = stmt.executeQuery("SELECT * FROM access_log;")){
                    while(resultSet.next()) {
                        AccessLog accessLog = new AccessLog();
                        accessLog.setId(resultSet.getInt(1));
                        accessLog.setName(resultSet.getString(2));
                        accessLog.setDescription(resultSet.getString(3));
                        accessLog.setTimestamp(resultSet.getDate(4));
                        accessLogDAO.add(accessLog);
                    }
                }
            }
        }
        catch (SQLException e){
            e.getMessage();
        }
        return accessLogDAO;
    }

    public void insertEntry(String name, String description){
        try(Connection connection = HikariConnectionPool.getConnection()) {
            try(PreparedStatement statement = connection.prepareStatement("INSERT INTO access_log (name, description) VALUES (?, ?)")){
                statement.setString(1, name);
                statement.setString(2, description);
                statement.executeUpdate();

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteEntry(String name){
        try(Connection connection = HikariConnectionPool.getConnection()) {
            try(PreparedStatement statement = connection.prepareStatement("DELETE FROM access_log WHERE name = ?;")){
                statement.setString(1, name);
                statement.executeUpdate();

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


//    public static void main(String[] args) {
//        AccessLogDAO ex = new AccessLogDAO();
//        ex.start();
//    }

}
