package ictgradschool.web.lab15.ex1;




import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;


public class LoggingTableNewEntry extends HttpServlet {

    @Override
    public void init() throws ServletException {
        super.init();
        ServletContext context = getServletContext();
        String fullPath = context.getRealPath("WEB-INF" + File.separator + "db.properties");
        HikariConnectionPool.setup(fullPath);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        AccessLogDAO accessLogDAO = new AccessLogDAO();
        String name = request.getParameter("name");
        String description = request.getParameter("enterDescription");
        accessLogDAO.insertEntry(name, description);
        response.sendRedirect("../question1");
        StringBuffer url = request.getRequestURL();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
        PrintWriter pw = response.getWriter();
        //response.sendRedirect("../question1");
        pw.print("<h1>405 - GET method not supported</h1>");

        pw.close();
    }
}
