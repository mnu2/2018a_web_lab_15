DROP TABLE IF EXISTS access_log;

CREATE TABLE IF NOT EXISTS access_log (
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(80),
    description VARCHAR(200),
    timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
);

INSERT INTO access_log (name, description) VALUES
    ('Shirly', 'Committed posted a photo of a cat'),
    ('John', 'Liked and commented on Shirly\'s photo'),
    ('Bjorn', 'Posted a gif of the northern lights'),
    ('Sigrid', 'Complained about Icelandic winters'),
    ('Jeremiah', 'Predicted when the messiah was returning - incorrectly');

