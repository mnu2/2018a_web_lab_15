package ictgradschool.web.lab15.ex1;

import sun.misc.Request;

import javax.servlet.Servlet;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class LoggingTable extends HttpServlet {

    @Override
    public void init() throws ServletException {
        super.init();
        ServletContext context = getServletContext();
        String fullPath = context.getRealPath("WEB-INF" + File.separator + "db.properties");
        HikariConnectionPool.setup(fullPath);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter pw = response.getWriter();
        pw.print("<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "    <head>\n" +
                "        <meta charset=\"UTF-8\">\n" +
                "        <title>ICT Graduate School - Web Lab 15</title>\n" +
                "    <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css\" integrity=\"sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4\" crossorigin=\"anonymous\"></head><body><div class='container'>");
        List<AccessLog> data = AccessLogDAO.loadLog();
        pw.print("<p>Display your table and form here</p>");
        pw.print("<form action='./question1/new' method='POST'>\n" +
                "  <div class=\"form-group\">\n" +
                "    <label for=\"Name\">Name</label>\n" +
                "    <input type=\"text\" class=\"form-control\" name=\"name\" aria-describedby=\"enterName\" placeholder=\"Enter Name\">\n" +
                "    <label for=\"Description\">Description</label>\n" +
                "    <input type=\"text\" class=\"form-control\" name=\"enterDescription\" placeholder=\"description\">\n" +
                "  </div>\n" +
                "  <button type=\"submit\" class=\"btn btn-primary\">Submit</button>\n" +
                "</form>");
        for (int i = 0; i < data.size(); i++){
            pw.print("<div class='row'>");
            pw.print("<div class='col-1 bg-warning'>" + data.get(i).getId() + "</div>");
            pw.print("<div class='col-1 bg-danger'>" + data.get(i).getName() + "</div>");
            pw.print("<div class='col-5 bg-warning'>" + data.get(i).getDescription() + "</div>");
            pw.print("<div class='col-2 bg-danger'>" + data.get(i).getTimestamp() + "</div>");
            pw.print("</div>");
        }
        pw.print("</table>");

        pw.print("</div><script src=\"https://code.jquery.com/jquery-3.3.1.slim.min.js\" integrity=\"sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo\" crossorigin=\"anonymous\"></script>\n" +
                "<script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js\" integrity=\"sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ\" crossorigin=\"anonymous\"></script>\n" +
                "<script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js\" integrity=\"sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm\" crossorigin=\"anonymous\"></script></body><html>");
        pw.close();
    }
}
